## Negative Senders:
### Sender: debra.perlingiere@enron.com

*Average Sentiment*: -0.0655419891393523

*Email Count*: 2278



*Most extreme sentiment email*:

*Subject*: 

*Body*: 

Marci said Julie, and Kyle are out I think.  You could try them.  What about
Pat?  I have not seen him in a long time.





Debra Perlingiere
Enron North America Corp.
1400 Smith Street, EB 3885
Houston, Texas 77002
Phone 713-853-7658
Fax  713-646-3490


*File Path*: /mails/perlingiere-d/sent/194.

*Sentiment*: -0.0999462157487869



### Sender: susan.bailey@enron.com

*Average Sentiment*: -0.0434992579886547

*Email Count*: 13



*Most extreme sentiment email*:

*Subject*: American Commercial Barge Line LLC

*Body*: 

All,

Attached is the template for Annex B & B-1, along with the LC form for the captioned Counterparty's Omnibus Confirmation for Deal No. YK1853.1.


Contact me with any questions.


Regards,
Susan S. Bailey
Senior Legal Specialist
Enron Wholesale Services
Legal Department
1400 Smith Street, Suite 3803A
Houston, Texas 77002
phone: (713) 853-4737
fax: (713) 646-3490
email: susan.bailey@ enron.com


*File Path*: /mails/bailey-s/sent_items/4.

*Sentiment*: -0.0978314280509949



### Sender: stephanie.panus@enron.com

*Average Sentiment*: -0.0330990901073584

*Email Count*: 26



*Most extreme sentiment email*:

*Subject*: e prime Energy Marketing, Inc.

*Body*: 

Attached for your further handling are Annex B, B-1, B-2 and B-3 for e prime Energy Marketing, Inc., deal no. YL0308.1/3/4.  Please call me if you have any questions.



Stephanie Panus
Enron Wholesale Services
ph:  713.345.3249
fax:  713.646.3490


*File Path*: /mails/panus-s/sent_items/30.

*Sentiment*: -0.100393086671829



### Sender: carol.clair@enron.com

*Average Sentiment*: -0.0118827336063575

*Email Count*: 1281



*Most extreme sentiment email*:

*Subject*: 

*Body*: 

Nidia:
This is not our current form.  I have asked Susan Bailey  to send you our
current form for this deal.

Carol St. Clair
EB 3892
713-853-3989 (Phone)
713-646-3393 (Fax)



Nidia Mendoza
06/12/2000 02:57 PM

To: Carol St Clair/HOU/ECT@ECT
cc:
Subject: LC format sent to Ferrell International Limited

Please see attached doc.





If this doesn't satisfy legal requirements, please send me the latest format
as soon as possible.

Thanks,

Nidia
x3-4868
fax:  713/853-4868


*File Path*: /mails/stclair-c/sent/683.

*Sentiment*: -0.100004740059376



### Sender: mark.mcconnell@enron.com

*Average Sentiment*: -0.00118504259355214

*Email Count*: 98



*Most extreme sentiment email*:

*Subject*: 

*Body*: 

Noel,

I would like to reserve a space on a van on Saturday Oct 20th for the Habitat for Humanity effort.

Let me know if you need additional information or if they are already full.  I can certainly make it out there on my own.

Thanks,

Mark

Mark McConnell
Transwestern Pipeline Company
713-345-7896   office
713-822-4862   cell
713-646-2551   fax
mark.mcconnell@enron.com


*File Path*: /mails/mcconnell-m/sent_items/41.

*Sentiment*: -0.0981998220086098



## Positive Senders:
### Sender: j.kaminski@enron.com

*Average Sentiment*: 0.145788236092868

*Email Count*: 611



*Most extreme sentiment email*:

*Subject*: Friday drinks

*Body*: 

Brian,

What about meeting around 5:50 - 6:00 p.m. at the Angelica Movie Center
on Smith. There is an underground parking between Smith and Louisiana
and the movie theater will validate your parking ticket.

There are several bars at the center.

The movie starts at 7:00 with some boring speeches. Saturday should be more
interesting - it's a comedy day.

Vince


*File Path*: /mails/kaminski-v/sent_items/1245.

*Sentiment*: 0.169652804732323



### Sender: kaminski@enron.com

*Average Sentiment*: 0.145453561756473

*Email Count*: 130



*Most extreme sentiment email*:

*Subject*: 

*Body*: 

Paula,

Thanks a lot. Will you drive on that day?

Vince


*File Path*: /mails/kaminski-v/sent_items/946.

*Sentiment*: 0.169644042849541



### Sender: vince.kaminski@enron.com

*Average Sentiment*: 0.10138460408879

*Email Count*: 2170



*Most extreme sentiment email*:

*Subject*: Please, call George Posey

*Body*: 

Osman,

George  Posey asked for a call.

Vince


*File Path*: /mails/kaminski-v/sent/1004.

*Sentiment*: 0.169559448957443



### Sender: e..haedicke@enron.com

*Average Sentiment*: 0.0812870323112825

*Email Count*: 82



*Most extreme sentiment email*:

*Subject*: Budget Meeting

*Body*: 

Type:Single Meeting
Organizer:Haedicke, Mark E.
Start Time:Friday, July 27, 2001 2:00 PM
End Time:Friday, July 27, 2001 2:30 PM
Time Zone:Central Time (US & Canada)
Location:EB3325

*~*~*~*~*~*~*~*~*~*


*File Path*: /mails/haedicke-m/sent_items/92.

*Sentiment*: 0.0934644341468811



### Sender: mark.fisher@enron.com

*Average Sentiment*: 0.0802118695880237

*Email Count*: 19



*Most extreme sentiment email*:

*Subject*: Work Request

*Body*: 

Fault Pareto Work Request From Tom Nemila


*File Path*: /mails/fischer-m/sent/19.

*Sentiment*: 0.09378731995821



