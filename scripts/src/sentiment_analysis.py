import psycopg2

def get_senders (asc=True):
    senders = {}
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='data'")
    cur = conn.cursor()
    sort_direction = "ASC" if asc else "DESC"
    cur.execute("""
        SELECT polarity, sender, count
        FROM (
                SELECT
                    AVG(predictions.polarity) AS polarity,
                    COUNT(*) AS count,
                    sender
                FROM mail
                JOIN predictions
                ON mail.id = predictions.id
                GROUP BY sender
        ) AS avg_polarity
        WHERE count > 10
        ORDER BY avg_polarity.polarity {sort_direction}
        LIMIT 5;
    """.format(sort_direction=sort_direction))
    for record in cur:
        senders[record[1]] = {
            "sentiment": record[0],
            "sender": record[1],
            "count": record[2],
        }

    for sender, _ in senders.items():
        cur.execute("""
            SELECT subject, body, path, predictions.polarity
            FROM mail
            JOIN predictions
            ON mail.id = predictions.id
            WHERE sender = %s
            ORDER BY predictions.polarity {sort_direction}
            LIMIT 1;
        """.format(sort_direction=sort_direction), (sender,))
        mail = cur.fetchone()
        senders[sender]["mail"] = {
            "subject": mail[0],
            "body": mail[1],
            "path": mail[2],
            "sentiment": mail[3],
        }
    conn.commit()
    cur.close()
    conn.close()
    return senders

def print_senders (senders):
    for _, sender in senders.items():
        print("### Sender: {}\n".format(sender["sender"]))
        print("*Average Sentiment*: {}\n".format(sender["sentiment"]))
        print("*Email Count*: {}\n".format(sender["count"]))
        print("\n")
        print("*Most extreme sentiment email*:\n")
        mail = sender["mail"]
        print("*Subject*: {}\n".format(mail["subject"]))
        print("*Body*: \n\n{}\n\n".format(mail["body"]))
        print("*File Path*: {}\n".format(mail["path"]))
        print("*Sentiment*: {}\n".format(mail["sentiment"]))
        print("\n")

negative_senders = get_senders(asc=True)
print("## Negative Senders:")
print_senders(negative_senders)

positive_senders = get_senders(asc=False)
print("## Positive Senders:")
print_senders(positive_senders)

