import torch
import psycopg2
import psycopg2.extras
from RNN import SimpleRNN, default_hidden_size, predict
from vocabulary import Vocabulary


def initialize_db ():
    with psycopg2.connect("dbname='postgres' user='postgres' host='data'") as conn:
        with conn.cursor() as cur:
            cur.execute("""
                CREATE TABLE IF NOT EXISTS predictions (
                    id integer PRIMARY KEY REFERENCES mail (id),
                    polarity float
                )
            """)
            conn.commit()

def get_mail_to_analyse_batch (batchsize):
    with psycopg2.connect("dbname='postgres' user='postgres' host='data'") as conn:
        with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            cur.arraysize = batchsize
            cur.execute("""
                SELECT id, tokens
                FROM mail
                WHERE id NOT IN (SELECT id FROM splits)
            """)
            while True:
                batch = cur.fetchmany()
                if not batch:
                    return
                yield batch

def insert_prediction(mail_id, polarity):
    with psycopg2.connect("dbname='postgres' user='postgres' host='data'") as conn:
        with conn.cursor() as cur:
            cur.execute("""
                INSERT INTO predictions
                (id, polarity)
                VALUES
                (%s, %s)
            """, (mail_id, polarity))
            conn.commit()

def predict_polarity(model, vocabulary):
    with torch.no_grad():
        for raw_batch in get_mail_to_analyse_batch(1):
            tokens = raw_batch[0]["tokens"]
            mail_id = raw_batch[0]["id"]

            batch = vocabulary.make_batch([tokens])
            for i in range(batch.size()[0]):
                sentence = batch[i]

                output = predict(model, sentence)
                prediction = output[0].item()
                insert_prediction(mail_id, prediction)
                print(".", end="", flush=True)
                del output

def load_model (vocabulary):
    path = "/models/v1"
    state_dict = torch.load(path)
    model = SimpleRNN(len(vocabulary), default_hidden_size)
    model.load_state_dict(state_dict)
    return model


def main ():
    print("Storing predictions")
    vocabulary = Vocabulary()
    vocabulary.build()
    initialize_db()
    model = load_model(vocabulary)
    predict_polarity(model, vocabulary)

main()
