import torch
import torch.nn as nn
import psycopg2
import psycopg2.extras
from RNN import SimpleRNN, default_hidden_size, predict
from vocabulary import Vocabulary


def get_raw_train_batch (batchsize):
    with psycopg2.connect("dbname='postgres' user='postgres' host='data'") as conn:
        with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            cur.arraysize = batchsize
            cur.execute("""
                SELECT mail.*
                FROM mail
                JOIN splits
                ON mail.id = splits.id
                WHERE splits.train
                ORDER BY random()
            """)
            while True:
                batch = cur.fetchmany()
                if not batch:
                    return
                yield batch


def get_raw_test_batch (batchsize):
    with psycopg2.connect("dbname='postgres' user='postgres' host='data'") as conn:
        with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            cur.arraysize = batchsize
            cur.execute("""
                SELECT mail.*
                FROM mail
                JOIN splits
                ON mail.id = splits.id
                WHERE splits.test
                ORDER BY random()
            """)
            while True:
                batch = cur.fetchmany()
                if not batch:
                    return
                yield batch

def train (vocabulary):
    print("Training model")
    rnn = SimpleRNN(len(vocabulary), default_hidden_size)
    criterion = nn.MSELoss()
    train_loss = 0
    learning_rate = 0.005
    train_losses = []
    test_losses = []
    max_iterations = 100
    for iteration in range(1, max_iterations):
        count = 0
        train_loss = 0
        old_state = rnn.state_dict()
        for raw_batch in get_raw_train_batch(10):
            count = count + 1
            batch = vocabulary.make_batch([row["tokens"] for row in raw_batch])
            labels = vocabulary.make_labels([row["polarity"] for row in raw_batch])
            batch_losses = 0
            for i in range(batch.size()[0]):
                rnn.zero_grad()
                sentence = batch[i]
                label = labels[i]
                
                output = predict(rnn, sentence)
                loss = criterion(output, label)
                batch_losses = batch_losses + loss.item()
                update_weights(rnn, loss, learning_rate)
            train_loss = train_loss + (batch_losses / batch.size()[0])
            print(".", end="", flush=True)

        train_losses.append(train_loss / count)
        test_loss = test_net(rnn, vocabulary, criterion)
        test_losses.append(test_loss)
        print("\n------------------")
        print("iteration", iteration)
        print("train_loss", train_loss / count)
        print("test_loss", test_loss)
        print("==================")
        if (
            len(test_losses) > 1 and
            test_losses[-1] >= test_losses[-2]
        ):
            # test data shows no more training improvement
            # reload previous state of model and return results
            rnn.load_state_dict(old_state)
            return rnn, train_losses[:-1], test_losses[:-1]
    return rnn, train_losses, test_losses

def update_weights(rnn, loss, learning_rate):
    loss.backward()
    for p in rnn.parameters():
        p.data.add_(-learning_rate, p.grad.data)

def test_net(rnn, vocabulary, criterion):
    losses = 0
    count = 0
    with torch.no_grad():
        for raw_batch in get_raw_test_batch(10):
            count = count + 1
            batch = vocabulary.make_batch([row["tokens"] for row in raw_batch])
            labels = vocabulary.make_labels([row["polarity"] for row in raw_batch])
            batch_losses = 0
            for i in range(batch.size()[0]):
                sentence = batch[i]
                label = labels[i]

                output = predict(rnn, sentence)
                loss = criterion(output, label)
                batch_losses = batch_losses + loss.item()
            losses = losses + batch_losses / batch.size()[0]
            print(",", end="", flush=True)

        return losses / count

def main ():
    # TODO: enable saving and loading vocabulary with model
    # to ensure exakt same vocabulary is used for training and prediction
    vocabulary = Vocabulary()
    vocabulary.build()
    model, train_losses, test_losses = train(vocabulary)
    path = "/models/v1"
    torch.save(model.state_dict(), path)
    torch.save(train_losses, path + ".train_losses")
    torch.save(test_losses, path + ".test_losses")

main()
