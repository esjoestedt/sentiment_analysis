import psycopg2
import psycopg2.extras

def create_split ():
    #TODO: use variables for thresholds
    with psycopg2.connect("dbname='postgres' user='postgres' host='data'") as conn:
        with conn.cursor() as cur:
            cur.execute("""
                CREATE TABLE IF NOT EXISTS splits (
                    id integer PRIMARY KEY REFERENCES mail (id),
                    train bool,
                    test bool,
                    validate bool
                );
            """)
            # will fail with UNIQUE constraint if table email id exists
            # FIXME: handle existing data splits better, maybe with a version/name column
            # and/or set and keep random seed
            cur.execute("""
                SELECT MIN(count) FROM ( 
                    SELECT COUNT(id) AS count FROM mail
                    WHERE polarity < -0.25
                    UNION
                    SELECT COUNT(id) AS count FROM mail
                    WHERE polarity > 0.25
                ) AS pos_neg_counts
            """)
            length = cur.fetchone()[0]
            # balance datasets by having the same number of positive and negative labels
            cur.execute("""
                INSERT INTO splits
                SELECT
                    id,
                    rand < .75 AS train,
                    rand >= .75 AS test
                FROM (
                    SELECT
                        id,
                        random() AS rand,
                        polarity
                    FROM mail
                    WHERE polarity < -0.25
                ) AS splitting
                LIMIT %s
            """, (length,))
            cur.execute("""
                INSERT INTO splits
                SELECT
                    id,
                    rand < .75 AS train,
                    rand >= .75 AS test
                FROM (
                    SELECT
                        id,
                        random() AS rand,
                        polarity
                    FROM mail
                    WHERE polarity > 0.25
                ) AS splitting
                LIMIT %s
            """, (length,))
            conn.commit()

create_split()
