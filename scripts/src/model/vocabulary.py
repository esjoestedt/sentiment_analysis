import psycopg2
import torch

class Vocabulary:

    def __init__(self):
        self.PAD = "<pad>"
        self.UNK = "<unk>" # represents unknown word
        self.num_tokens = 25000

    def _load (self):
        with psycopg2.connect("dbname='postgres' user='postgres' host='data'") as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        token,
                        COUNT(token) AS count
                    FROM (
                        SELECT unnest(tokens) AS token
                        FROM mail
                    ) AS tokens
                    GROUP BY token
                    ORDER BY count DESC
                    LIMIT %s;
                """, (self.num_tokens,));
                self._vocabulary = [self.PAD, self.UNK] + [token[0] for token in cur.fetchall()]

    def _index (self):
        self._idx_to_token = {idx: token for idx, token in enumerate(self._vocabulary)}
        self._token_to_idx = {token: idx for idx, token in enumerate(self._vocabulary)}

    def build (self):
        self._load()
        self._index()

    def __len__ (self):
        # tokens + PAD + UNK
        return self.num_tokens + 2

    def make_batch (self, sentences):
        max_len = max(len(tokens) for tokens in sentences)
        assert max_len > 0 and len(sentences) > 0
        # zero is the pad index
        batch = torch.zeros(len(sentences), max_len, 1, len(self._vocabulary))

        for row_idx, tokens in enumerate(sentences):
            for token_idx, token in enumerate(tokens):
                vocab_idx = self._token_to_idx.get(token, 1) # fall back to UNK
                batch[row_idx][token_idx][0][vocab_idx] = 1
        return batch

    def make_labels (self, polarities):
        labels = torch.zeros(len(polarities), 1, 1)

        for row_idx, _ in enumerate(labels):
            labels[row_idx][0] = polarities[row_idx]
        return labels
