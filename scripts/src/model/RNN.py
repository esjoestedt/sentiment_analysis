import torch
import torch.nn as nn

default_hidden_size = 200

class SimpleRNN(nn.Module):

    def __init__ (self, input_size, hidden_size):
        super(SimpleRNN, self).__init__()
        output_size = 1 # polarity
        self.hidden_size = hidden_size

        self.i2h = nn.Linear(input_size + hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size + hidden_size, output_size)
        self.shrink = nn.Tanh()

    def forward (self, input, hidden):
        combined = torch.cat([input, hidden], 1)
        hidden = self.i2h(combined)
        output = self.shrink(self.i2o(combined))
        return output, hidden
    
    def init_hidden (self):
        return torch.zeros(1, self.hidden_size)


class SimpleRNN2(nn.Module):

    def __init__ (self, input_size, hidden_size):
        super(SimpleRNN2, self).__init__()
        output_size = 1 # polarity
        self.hidden_size = hidden_size

        self.i2h = nn.Linear(input_size + hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size + hidden_size, output_size)
        self.o2o = nn.Linear(hidden_size + output_size, output_size)
        self.dropout = nn.Dropout(0.1)
        self.shrink = nn.Tanh()

    def forward (self, input, hidden):
        combined = torch.cat([input, hidden], 1)
        hidden = self.i2h(combined)
        output = self.i2o(combined)
        output = self.o2o(torch.cat([hidden, output], 1))
        output = self.dropout(output)
        output = self.shrink(output)
        return output, hidden

    def init_hidden (self):
        return torch.zeros(1, self.hidden_size)

def predict (rnn, sentence):
    hidden = rnn.init_hidden()
    for j in range(sentence.size()[0]):
        output, hidden = rnn.forward(sentence[j], hidden)
    return output
