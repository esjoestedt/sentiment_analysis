import email
from glob import glob
from textblob import TextBlob
import re
from datetime import datetime

def get_files ():
    return [path for path in glob("/mails/**/*sent*/*.") if not "/presentations/" in path]

def parse_mails (paths):
    for path in paths:
        with open(path) as fp:
            try:
                mail = email.message_from_file(fp)
            except Exception:
                print("Failed to parse file '{}'".format(path))
                continue
            try:
                yield parse_mail(mail, path)
            except Exception as e:
                print("Failed to parse contents of file '{}'".format(path))
                print(e)

def parse_mail (mail, path):
    if (mail.is_multipart()):
        raise Exception("Multipart mails not supported")
    subject = parse_subject(mail.get("Subject"))
    body = parse_body(mail.get_payload())
    polarity, subjectivity, tokens = get_sentiment_and_tokens(subject, body)
    datetime_sent = parse_datetime(mail.get("Date"))
    return {
        "sender": mail.get("From"),
        "polarity": polarity,
        "subjectivity": subjectivity,
        "subject": subject,
        "orig_subject": mail.get("Subject"),
        "body": body,
        "tokens": tokens,
        "path": path,
        "datetime_sent": datetime_sent,
    }

def parse_subject (subject):
    if len(subject) <= 4:
        return subject
    elif should_discard_subject(subject):
        return ""
    return subject

def should_discard_subject(subject):
    return (
        subject[:4] == "RE: " or
        subject[:4] == "Re: " or
        subject[:4] == "FW: " or
        subject[:5] == "Fwd: "
    )

def parse_body (body):
    lines = body.split("\n")
    cleansed = []
    for line in lines:
        if should_discard_rest(line):
            break
        cleansed.append(str.strip(line))
    return str.strip("\n".join(cleansed))

best_pattern = re.compile("[,.;: -]*best(\s*regards)?([,.;: -]*)?$", re.IGNORECASE)
def should_discard_rest(line):
    return (
        "-Original Message-" in line or
        "- Forwarded by" in line or
        (len(line) > 6 and line[:6] == "From: ") or
        best_pattern.match(line)
    )

not_word_pattern = re.compile("^[.,:;='\"+<>0-9/-]+$")
def get_sentiment_and_tokens (subject, body):
    blob = TextBlob(subject + "\n" +  body).lower()
    if not blob.words or not TextBlob(body).words:
        raise Exception("this mail is empty")
    words = [word for word in blob.words if not not_word_pattern.match(word)]
    if not words:
        raise Exception("this mail is empty")
    return (blob.sentiment.polarity, blob.sentiment.subjectivity, words)

def parse_datetime (datetime_sent_str):
    format_str = "%a, %d %b %Y %H:%M:%S %z"
    datetime_sent_str = " ".join(datetime_sent_str.split(" ")[:-1]) # remove last part "(PST)" of date time string
    return datetime.strptime(datetime_sent_str, format_str)
