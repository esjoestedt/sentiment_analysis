from .parse import parse_subject, parse_body

def test_parse_subject_discards_RE ():
    original = "RE: Some subject"
    cleansed = ""
    assert parse_subject(original) == cleansed

def test_parse_subject_removes_deep_RE ():
    original = "RE: RE: Some subject"
    cleansed = ""
    assert parse_subject(original) == cleansed

def test_parse_subject_removes_Re ():
    original = "Re: Some subject"
    cleansed = ""
    assert parse_subject(original) == cleansed

def test_parse_subject_removes_FW ():
    original = "FW: Some subject"
    cleansed = ""
    assert parse_subject(original) == cleansed

def test_parse_subject_removes_deep_FW ():
    original = "RE: FW: Some subject"
    cleansed = ""
    assert parse_subject(original) == cleansed

def test_parse_subject_removes_fWd ():
    original = "Fwd: Some subject"
    cleansed = ""
    assert parse_subject(original) == cleansed

def test_parse_body_removes_original_message_part ():
    original = """
        text from sender        

        ----Original Message-----
        text that should be discarded 
    """
    cleansed = str.strip("""
        text from sender        

    """)
    assert parse_body(original) == cleansed

def test_parse_body_removes_from_part ():
    original = "\n".join([
        "text from sender",
        "",
        "From: someone@somewhere",
        "text that should be discarded",
    ])
    cleansed = "text from sender"
    assert parse_body(original) == cleansed

def test_parse_body_removes_from_part ():
    original = "\n".join([
        "text from sender",
        "",
        "--- Forwarded by someone"
        "text that should be discarded",
    ])
    cleansed = "text from sender"
    assert parse_body(original) == cleansed


# "best" is a positive word that should not be considered
# part of the sender's sentiment when in a closing statement
def test_parse_body_removes_best_closings ():
    original = "\n".join([
        "text from sender",
        "",
        "Best",
        "more closing",
    ])
    cleansed = "text from sender"
    assert parse_body(original) == cleansed

def test_parse_body_removes_best_regards_closings ():
    original = "\n".join([
        "text from sender",
        "",
        "best regards,",
        "more closing",
    ])
    cleansed = "text from sender"
    assert parse_body(original) == cleansed
