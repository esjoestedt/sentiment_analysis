import psycopg2

def initialize ():
    with psycopg2.connect("dbname='postgres' user='postgres' host='data'") as conn:
        with conn.cursor() as cur:
            cur.execute("""
                CREATE TABLE IF NOT EXISTS mail (
                    id serial PRIMARY KEY,
                    polarity float,
                    subjectivity float,
                    sender varchar,
                    subject varchar,
                    orig_subject varchar,
                    body varchar,
                    tokens varchar[],
                    path varchar,
                    datetime_sent timestamptz,
                    UNIQUE(
                        sender,
                        orig_subject,
                        datetime_sent
                    )
                );
            """)
            conn.commit()

# TODO: stop using this context based class
class MailInserter ():

    def __enter__ (self):
        return self

    def insert (self, mail):
        with psycopg2.connect("dbname='postgres' user='postgres' host='data'") as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute("""
                        INSERT INTO mail (
                            polarity,
                            subjectivity,
                            sender,
                            subject,
                            orig_subject,
                            body,
                            tokens,
                            path,
                            datetime_sent
                        ) VALUES (
                            %s,
                            %s,
                            %s,
                            %s,
                            %s,
                            %s,
                            %s,
                            %s,
                            %s
                        );
                    """, (
                        mail["polarity"],
                        mail["subjectivity"],
                        mail["sender"],
                        mail["subject"],
                        mail["orig_subject"],
                        mail["body"],
                        mail["tokens"],
                        mail["path"],
                        mail["datetime_sent"],
                    ))
                    conn.commit()
                except Exception as e:
                    print("""
                        Failed to insert {}\n
                        Error: {}
                    """.format(mail, e))

    def __exit__ (self, *args):
        pass

