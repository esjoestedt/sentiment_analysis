from import_mails import parse, load_db

print("Starting to import mails...")

try:
    load_db.initialize()
    mail_files = parse.get_files()
    with load_db.MailInserter() as inserter:
        for mail in parse.parse_mails(mail_files):
            inserter.insert(mail)
    print("Done importing mails...")
except Exception as e:
    print("Failed importing mails...")
    print(e)
