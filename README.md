# Sentiment Analysis

This repo deals with doing sentiment analysis on the [Enron mail dataset](https://www.cs.cmu.edu/~./enron/).

The purpose is to extract a list of the five most negative and positive email senders respectively.

The repo has two containers: one with scripts for importing the data and making the sentiment analysis and the other for hosting the database.


## The scripts container

This contains the Python scripts for importing mail data and doing the sentiment analysis.
The environment variable `SA_MAILS_PATH` must be exported and set to the directory with the mail files.
e.g. 

`export SA_MAILS_PATH=/home/erik/Projects/EnronSentimentAnalysis/maildir/`


## The data directory

This directory contains two folders:

* pgdata: Postgres DB data
* models: stored trained models


## Importing the mail

The mails are imported into a Postgresql database table, that has the following fields:

* id - incremented primary key
* polarity - a polarity score from TextBlob ranging from -1 to 1 - for training label
* subjectivity - a subjectivity score from TextBlob - not used
* subject - the parsed email subject
* orig_subject - the original email subject
* body - the parsed email body
* tokens - the tokens (words) parsed from subject + body - used as training input
* sender - the sender of the email
* path - the original path to the email file
* datetime_sent - the time the mail was sent

I take all mails from the "\*sent\*" folders, excluding "\*/presentations/\*".  

The mails are stored in a Postgres database, as is the information about train/test dataset splits and predictions.

The mails table has a UNIQUE constraint, that guarantees that no email are duplicates based on the `From`, `Subject` and `Date` fields. This enables me to read all mails from the "\*sent\*" folders, although they contain duplicates.

The mail import script can be run with:

`docker-compose run --rm scripts python run_import.py`

If the script fails because it cannot connect to the data container, try again, or start the data container manually with
`docker-compose up -d data`

## Training / testing data.

The mails are split up into training and testing sets. I'm doing no post-training validation - basically the testing set acts as validation. I'm also using the test set for deciding when to stop training. It would be easy to add a validation set and split the data into those three sets, but I didn't put time on that. Once the model has been trained, its loss on the test set shows how well it performs.


The data is split with the split_data.py script:

`docker-compose run --rm scripts python model/split_data.py`

It creates a database table `splits` and chooses all mails that has polarity < -0.25 or polarity > 0.25 and splits them 75/25 into two polarity-balanced sets.
I chose to restrict training data to more "distinct" polarities in the hopes that it would help the model understand the concept better. The analysis is later performed on all mails that are not in the any of the training or testing sets.


## Training labels

I use TextBlob to create source-of-truth labels for the mail data. This is a hack - obviously in a real scenario I would have to label the mails in some other way. The current label choice means that I'm effectively trying to model TextBlob's sentiment analysis function with a simple RNN.


## Input encoding

The indata to the model is the email text contents. The text is parsed into a list of its tokens, and each token in the list is converted to a *one-hot* encoded vector. The one-hot encoding is based on the 25000 most common words in all mails, and two extra words: padding and "unknown word" (for words outside the 25000 most common ones). Each word gets encoded by a 25002 elements vector, with all zeros except a '1' at the index of the corresponding word. In this way, a list of words gets encoded into a list of vectors with ones and zeros, suitable as input to an ANN.


## The model

There seems to be two paths one can take to doing sentiment analysis: a grammatical knowledge based model and a supervised learning model trained on labeled data. I chose to the supervised learning model. Since an email has variable length, I chose to implement a Recurrent Neural Network: such a model can take a stream of data as consecutive input, and return a final prediction once the stream has ended. It does this by accepting both a chunk of data and a state context as input, and outputting both an intermediate prediction and a (modified) state context.

I chose to implement a very simple RNN, taken from a [PyTorch tutorial](https://pytorch.org/tutorials/intermediate/char_rnn_classification_tutorial.html). This model does not perform well, but I chose it because I don't have any previous experience with PyTorch or RNN's, so the model is on a level that I can understand. With more time, I would read up on GRU's or LSTM's to be able to improve the prediction performance. I also didn't use PyTorch's built-in RNN, but implemented the RNN from scratch, like in the tutorial. This was a chance for me to get more familiar with RNN's, and a necessary first step for me to be able to create a good model, were I to continue work on this.

The RNN has two linear layers, one for output and one for the hidden state vector; both layers recieve the concatenation of the input and hidden vectors. The output pipe also has a Tanh squashing function, to force the output to be between -1 and 1 (in the tutoral they use a LogSoftmax layer, because they are doing classification). The image in [the tutorial](https://pytorch.org/tutorials/intermediate/char_rnn_classification_tutorial.html#creating-the-network) illustrates the network.

I didn't manage to get batch training to work. I struggled some with the PyTorch RNN's and also Embeddings, but didn't make it work the first few attempts, so, I chose to run each sentence at a time. I did however implement batch loading of data from the database. This would enable me to implement batch training, testing and predicting with more time.

Because I didn't have batch training, I could not easily try other optimization methods, that torch supports. That, together with a better model, would potentially have enabled me to get better performance.

I also didn't try using CUDA, although my laptop has a GPU that supports it.

The model does not perform well, but I claim that I have set up a starting framework for
* importing raw mails
* training
* testing
* predicting

What remains is to research and implement a better model, try other optimization methods, fine tune hyperparameters and implement batch proccessing.

In my experience with implementing object localization for Diablo II, using Lua Torch CNN's, I know that I can get a model to work very well with a good amount of quality data, but it requires that I take time to familiarize myself deeply with a, to me, completely new subject.

The model is trained with the train.py script:

`docker-compose run --rm scripts python model/train.py`


### Performance

The simple RNN is trained over all batches, and then evaluate it with the test batches. I iterate this for as long as the model improves based on the testing. Since the loss function is MSE, and the output and label is a scalar ranging from -1 to 1, the maximum squared error (loss) is 2^2 = 4. If the model chose to respond '0' to all input, it would have a maximum loss of 1 (the loss would be equal to the population variance of the data).

Looking at the test data I used, it has the following descriptives (rounded):

- *average*: -0.0204
- *standard deviation*: 0.4767

(the train data descriptives look very similar) 

We can see that the training and testing data doesn't capture the range -1 to 1 perfectly.

The training ran for three iterations (the fourth didn't improve the performance, and was "rolled back"). The final test loss (rounded) is 0.2261. The test loss in the first iteration is 0.2288. Obviously the model is not learning very well: I would have hoped to reach a much lower test loss.

Since the MSE and variance are average sums of squares, the former of the deviation by the predictor to the true values, and the latter by the deviation of the average to the true values, they can be compared. Taking the square root of the test MSE loss gives us 0.4792; this can be compared to the square root of the variance: the standard deviation.

We can see that the square root of the MSE is essentially equal to the standard deviation, meaning that our predictor does not describe the data any better than what we would get from simply using the average. Thus, the predictor performs badly, and a more computationally efficient predictor would be to simply use the average, where we could expect a corresponding error equal to the variance. The average is very close to zero; using zero (no sentiment) as a constant prediction would be as good as using this model.


### The loss function

Since TextBlob calculates polarity scores as floats ranging from -1 to 1, I chose to see this as a regression problem. I chose the simplest loss function for that: mean squared error. Perhaps I would have gotten better results if I chose to see his as a classification problem with two classes: positive or negative sentiment, and used a good loss function for such a classification.


## Data Quality

I put some time in cleaning the email data, in the stage where I'm importing the mail data to the database.


### Remove subjects that are not authored by the sender:

Subjects like "RE: Great news" or "FW: Low sentiment" are just reproducing subjects from another author, therefore I discard them.


### Remove propagated text parts

Texts that are forwarded or otherwise propagated are not authored by the sender.

This cleaning has tests, and can be run with:

`docker-compose run --rm scripts pytest`


### Remove empty mails

The mails that, after previous parsing, turns out to be empty of information, are discarded.


## Sentiment analysis

The list of the five (5) most negative email senders is queried from the database like this:
```sql
		SELECT polarity, sender, count
		FROM (
				SELECT
						AVG(predictions.polarity) AS polarity,
						COUNT(*) AS count,
						sender
				FROM mail
				JOIN predictions
				ON mail.id = predictions.id
				GROUP BY sender
		) AS avg_polarity
		WHERE count > 10
		ORDER BY avg_polarity.polarity ASC
		LIMIT 5;
```

The corresponding list of the most positive can be found by reversing the sort order.

The predictions are made with the predict.py script:

`docker-compose run --rm scripts python model/predict.py`

The complete report of the sentiment analysis is [here](sentiment_analysis.md).


## Summary

The performance of the model is bad, comparable to what you get from using the average of the training data as a constant estimated prediction. The model needs to be more advanced to have better performance.

I claim to have set up a development environment for iteratively improving the model, by testing out more complex RNN's. To be able to do this I would need to read up on such RNN's, and learn how to better use PyTorch's features.

In my setup I have implemented some measures for guaranteeing data quality; I think this is very important, and something that must be in place before one can develop a good model.

I'm using a database to store the data, which enables me to have large amounts of data. One problem is that training and prediction is very slow. That could be fixed by using GPU's (CUDA), and imporving the use of the database.
